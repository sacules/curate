package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/gocolly/colly"
	"github.com/schollz/progressbar/v3"
)

type image struct {
	title  string
	author string
	url    string
}

func congressGet(imgURLs []string) []image {
	// FIXME: Use channels instead!
	imglock := new(sync.Mutex)

	images := make([]image, 0, len(os.Args[1:]))

	c := colly.NewCollector(colly.Async(true), colly.MaxDepth(2))
	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 2})

	c.OnHTML("#main_body", func(e *colly.HTMLElement) {
		img := image{}

		e.ForEach("#info>#title", func(n int, e *colly.HTMLElement) {
			img.title = e.Text
		})

		e.ForEachWithBreak("#info>#bib>ul>li>span>a", func(n int, e *colly.HTMLElement) bool {
			author := strings.Split(e.Text, ", approximately")
			img.author = author[0]
			return false
		})

		e.ForEach("#item>p>a", func(n int, e *colly.HTMLElement) {
			if !strings.Contains(e.Text, "TIFF") {
				return
			}
			img.url = e.Attr("href")
		})

		if img.author == "" {
			img.author = "Unknown"
		}

		imglock.Lock()
		images = append(images, img)
		imglock.Unlock()
	})

	c.OnError(func(r *colly.Response, e error) {
		log.Println("curate:", e, r.Request.URL, string(r.Body))
	})

	for _, site := range imgURLs {
		if !strings.Contains(site, "loc.gov") {
			continue
		}

		fmt.Println("Found", site)
		c.Visit(site)
	}
	c.Wait()

	return images
}

func pubDomainGet(url string) []string {
	var links []string

	c := colly.NewCollector()

	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		if !strings.Contains(e.Text, "Source") {
			return
		}

		links = append(links, e.Attr("href"))
	})

	fmt.Println("Visiting", url)
	c.Visit(url)

	return links
}

func download(images []image, downDir string) error {
	for i, img := range images {
		filename := fmt.Sprintf("%s - %s.tif", img.author, img.title)
		if len(filename) > 256 {
			filename = filename[:200] + "....tif"
		}

		if downDir != "" {
			os.Mkdir(downDir, 0755)
			filename = downDir + "/" + filename
		}

		// Doesn't properly work?
		if _, err := os.Stat(filename); os.IsExist(err) {
			continue
		}

		resp, err := http.Get("https:" + img.url)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		f, err := os.Create(filename)
		if err != nil {
			return err
		}

		msg := fmt.Sprintf("[green][%d/%d][reset] Downloading...", i+1, len(images))
		bar := progressbar.NewOptions(
			int(resp.ContentLength),
			progressbar.OptionEnableColorCodes(true),
			progressbar.OptionSetDescription(msg),
			progressbar.OptionShowBytes(true),
		)

		_, err = io.Copy(io.MultiWriter(f, bar), resp.Body)
		if err != nil {
			return err
		}

	}
	return nil
}

func main() {
	if len(os.Args) == 1 {
		fmt.Println("needs a link or more")
		os.Exit(1)
	}

	var pubDomLink, downDir string
	flag.StringVar(&pubDomLink, "p", "", "link to a publicdomainreview post")
	flag.StringVar(&downDir, "d", "", "download directory")
	flag.Parse()

	links := flag.Args()

	if pubDomLink != "" {
		links = pubDomainGet(pubDomLink)
	}

	images := congressGet(links)
	download(images, downDir)
}
